# -*- coding: utf-8 -*-
import RPiSystemInfo as sysinfo
from lib import weather_json, lcd, generallcd, adafruitlcd
import datetime
import psutil
import signal
import sys
import time
import threading


class RPISystemInfo:
    LCD = None

    def __init__(self):
        # TODO: start a thread listening for this and killing it...
        signal.signal(signal.SIGINT, self.exit_gracefully)
        self.display_init()

    def display_init(self):

        if sysinfo.USE_ADAFRUIT:
            self.LCD = adafruitlcd.AdafruitLCD(sysinfo.COLUMNS, sysinfo.ROWS)
        else:
            if sysinfo.METHOD:
                self.LCD = generallcd.GeneralLCD(i2c_expander=sysinfo.I2C_EXPANDER, i2c_address=sysinfo.I2C_ADDRESS,
                                                 columns=sysinfo.COLUMNS, rows=sysinfo.ROWS, dotsize=sysinfo.DOTSIZE,
                                                 auto_linebreaks=sysinfo.AUTO_LINEBREAKS,
                                                 backlight=sysinfo.BACKLIGHT)
            else:
                self.LCD = generallcd.GeneralLCD(gpio_rs=sysinfo.GPIO_RS, gpio_e=sysinfo.GPIO_E,
                                                 gpio_data=sysinfo.GPIO_DATA,
                                                 columns=sysinfo.COLUMNS, rows=sysinfo.ROWS, dotsize=sysinfo.DOTSIZE,
                                                 charmap=sysinfo.CHARMAP,
                                                 auto_linebreaks=sysinfo.AUTO_LINEBREAKS)

    def show_cpu_page(self, showfor):

        t_end = time.time() + showfor + 1

        self.LCD.color(lcd.Color.BLUE)

        while time.time() < t_end:

            cpu_info = psutil.cpu_percent(percpu=True)

            cpu_string_1 = "CPU:  "
            cpu_string_2 = " " * len(cpu_string_1)
            cpu_0 = "{:3d}%".format(int(cpu_info[0]))
            cpu_string_1 += cpu_0

            if sysinfo.THREAD_NUMBER > 1:
                cpu_1 = "{:3d}%".format(int(cpu_info[1]))
                cpu_string_1 += " " * (self.LCD.columns - len(cpu_1) - len(cpu_string_1)) + cpu_1
            if sysinfo.THREAD_NUMBER > 2:
                cpu_2 = "{:3d}%".format(int(cpu_info[2]))
                cpu_string_2 += cpu_2
            if sysinfo.THREAD_NUMBER > 3:
                cpu_3 = "{:3d}%".format(int(cpu_info[3]))
                cpu_string_2 += " " * (self.LCD.columns - len(cpu_3) - len(cpu_string_2)) + cpu_3

            self.LCD.write_line(cpu_string_1, 0)
            self.LCD.write_line(cpu_string_2, 1)

            time.sleep(1)

        self.LCD.clear()

    def show_mem_page(self, showfor):
        t_end = time.time() + showfor + 1

        #self.LCD.color(lcd.Color.WHITE)
        #self.LCD.write_line("Used:", 0)
        #self.LCD.write_line("Free:", 1)
        #
        # while time.time() < t_end:
        #    mem_string = "{:.1f}%".format(psutil.virtual_memory().percent)
        #    LCD.cursor_pos = (0, 16 - len(mem_string))
        #    LCD.write_string(mem_string)
        #
        #    free_string = "{} MB".format(psutil.virtual_memory().free >> 20)
        #    LCD.cursor_pos = (1, 16 - len(free_string))
        #    LCD.write_string(free_string)
        #
        #    time.sleep(1)
        #
        # LCD.clear()

        pass

    def show_service_page(self, showfor, service_name):
        t_end = time.time() + showfor + 1

        # TODO: Does this already work? why is it commented out?
        # service_info = proc_info.getSystemDinfo(service_name).split("\n")
        # pattern_status = re.compile(r"[A-Z](.*)(since)?(.*)")
        # service_status = pattern_status.search(service_info[2]).group().split("since")
        #
        # if len(service_status) == 2:
        #    pattern_time = re.compile(r";(.*)")
        #    service_time = pattern_time.search(service_status[1]).group()[1:]
        #    service_string = "{}, {}".format(service_status[0], service_time)
        #
        # else:
        #    service_string = service_status[0]
        #
        # framebuffer = ["{}".format(service_name).title(), ""]
        #
        # while time.time() < t_end:
        #    lcd.loop_string(service_string, LCD, framebuffer, 1, delay=0.1)
        #
        # LCD.clear()

        pass

    def show_weatherwarning_page(self, time_mark, repeats):

        if time.time() - time_mark >= 900:
            time_mark = time.time()
            threading.Thread(target=weather_json.update, args=(sysinfo.WEATHER_INFO,)).start()

        warnings = weather_json.get_warnings()

        if warnings:
            for warning in warnings:

                self.LCD.clear()

                strongest_severity = "Minor"
                event_texts = "***"
                event_location = warning.location + ":"

                for event in warning.weather_events:
                    event_texts += " " + event.event + ", "
                    severity = event.severity
                    if severity is not "Minor":
                        if strongest_severity is "Moderate":
                            strongest_severity = severity
                
                if strongest_severity is "Minor":
                    self.LCD.color(lcd.Color.YELLOW)
                elif strongest_severity is "Moderate":
                    self.LCD.color(lcd.Color.RED)
                else:
                    self.LCD.color(lcd.Color.VIOLET)

                event_texts = event_texts[:-2] + " ***"

                event_texts = event_texts.replace('Ä', 'AE').replace('Ö', 'OE').replace('Ü', 'UE') \
                    .replace('ä', 'ae').replace('ö', 'oe').replace('ü', 'ue')
                event_location = event_location.replace('Ä', 'AE').replace('Ö', 'OE').replace('Ü', 'UE') \
                    .replace('ä', 'ae').replace('ö', 'oe').replace('ü', 'ue')

                self.LCD.write_line(event_location, 0)
                self.LCD.loop_line(event_texts, 1, repeats, 4)

        else:
            self.LCD.color(lcd.Color.GREEN)
            self.LCD.write_line("Warnungen:", 0)
            self.LCD.loop_line("*** Keine Warnungen ***", 1, 2, 4)

        self.LCD.clear()

        return time_mark

    def exit_gracefully(self, signum, frame):
        self.LCD.close(True)
        sys.exit("Exited by signal {}".format(signum))

    def mainloop(self):

        time_mark = time.time()
        threading.Thread(target=weather_json.update, args=(sysinfo.WEATHER_INFO,)).start()

        # We have signal handling for stopping
        while True:
            hour = datetime.datetime.time(datetime.datetime.now()).hour
            if hour > sysinfo.NIGHT[0] or hour < sysinfo.NIGHT[1]:
                self.LCD.backlight(False)
            else:
                self.LCD.backlight(True)

            self.show_cpu_page(sysinfo.SHOW_PAGES_FOR)
            self.show_mem_page(sysinfo.SHOW_PAGES_FOR)
            for service in sysinfo.SERVICES:
                self.show_service_page(sysinfo.SHOW_PAGES_FOR, service)
            time_mark = self.show_weatherwarning_page(time_mark, sysinfo.SHOW_PAGES_FOR)
