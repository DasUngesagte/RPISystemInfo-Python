#!/usr/bin/python3
import adafruit_character_lcd.character_lcd_rgb_i2c as rgb_lcd
import board
import busio
from lib import lcd


class AdafruitLCD:
    lcd = None
    i2c = None
    rows = 2
    columns = 16

    def __init__(self, columns=16, rows=2):

        self.rows = rows
        self.columns = columns
        self.i2c = busio.I2C(board.SCL, board.SDA)
        self.lcd = rgb_lcd.Character_LCD_RGB_I2C(self.i2c, columns, rows)
        self.lcd.cursor = False
        self.color(lcd.Color.RED)
        self.clear()

    def write_line(self, string, row):
        for i in range(0, row):
            string = "\n" + string
        self.lcd.message = string

    def loop_line(self, string, row, repeats, padding, delay=0.25):
        if row > self.rows:
            raise Exception("Illegal row")
        if repeats < 1:
            repeats = 1

        # Start with empty spaces:
        complete_string = ' ' * self.columns

        # Add text and padding
        for i in range(repeats - 1):
            complete_string += string + ' ' * padding

        # Add the last text + fill with empty spaces for complete fade-out
        complete_string += string + ' ' * self.columns

        for i in range(len(complete_string) - self.columns):
            buffer_string = complete_string[i:i + self.columns]
            self.write_line(buffer_string, row)

        # Clear just the used line:
        self.clear_line(row)

    def clear_line(self, row):
        string = ' ' * self.columns
        for i in range(0, row):
            string = "\n" + string
        self.lcd.message = string

    def close(self, clear):
        if clear:
            self.lcd.clear()
        self.lcd.display = False

    def backlight(self, backlight):
        self.lcd.backlight_enabled = backlight

    def clear(self):
        self.lcd.clear()

    def color(self, color):
        if isinstance(color, lcd.Color):
            self.lcd.color = color.value
