#!/usr/bin/python3
import subprocess

def getSystemDinfo(servicename):
	cmd = "/bin/systemctl status {}.service".format(servicename)
	proc = subprocess.Popen(cmd, shell=True,stdout=subprocess.PIPE)
	return proc.communicate()[0].decode('utf-8')

def getDiskUsage():
	#TODO: Implement, maybe "df -h"
	pass
