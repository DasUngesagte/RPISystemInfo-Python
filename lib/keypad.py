from enum import Enum
#import board
#import digitalio
#import gamepad
#import time

class Actions(Enum):
    NONE = 0
    TOGGLE_BACKLIGHT = 1
    DISABLE_BACKLIGHT = 1
    ENABLE_BACKLIGHT = 1
    PAGE_UP = 2
    PAGE_DOWN = 3

class Buttons(Enum):
    SELECT = 0
    LEFT = 1
    RIGHT = 2
    UP = 3
    DOWN = 4


# TODO: Implement Action-listener-like button handling, maybe different thread
