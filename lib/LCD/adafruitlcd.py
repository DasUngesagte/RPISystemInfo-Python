#!/usr/bin/python3
# import adafruit_character_lcd.character_lcd_rgb_i2c as rgb_lcd
# import board
# import busio
from lib.LCD import lcd
import time

try:
    import smbus2 as smbus
except:
    import smbus as smbus

# LCD Address
ADDRESS = 0x20

# commands
LCD_CLEARDISPLAY = 0x01
LCD_RETURNHOME = 0x02
LCD_ENTRYMODESET = 0x04
LCD_DISPLAYCONTROL = 0x08
LCD_CURSORSHIFT = 0x10
LCD_FUNCTIONSET = 0x20
LCD_SETCGRAMADDR = 0x40
LCD_SETDDRAMADDR = 0x80

# flags for display entry mode
LCD_ENTRYRIGHT = 0x00
LCD_ENTRYLEFT = 0x02
LCD_ENTRYSHIFTINCREMENT = 0x01
LCD_ENTRYSHIFTDECREMENT = 0x00

# flags for display on/off control
LCD_DISPLAYON = 0x04
LCD_DISPLAYOFF = 0x00
LCD_CURSORON = 0x02
LCD_CURSOROFF = 0x00
LCD_BLINKON = 0x01
LCD_BLINKOFF = 0x00

# flags for display/cursor shift
LCD_DISPLAYMOVE = 0x08
LCD_CURSORMOVE = 0x00
LCD_MOVERIGHT = 0x04
LCD_MOVELEFT = 0x00

# flags for function set
LCD_8BITMODE = 0x10
LCD_4BITMODE = 0x00
LCD_2LINE = 0x08
LCD_1LINE = 0x00
LCD_5x10DOTS = 0x04
LCD_5x8DOTS = 0x00

# flags for backlight control
LCD_BACKLIGHT_R = 0b01000000  # GPA6
LCD_BACKLIGHT_G = 0b10000000  # GPA7
LCD_BACKLIGHT_B = 0b00000001  # GPB0

GPIOB_EN = 0b00100000  # Enable bit
GPIOB_RW = 0b01000000  # Read/Write bit
GPIOB_RS = 0b10000000  # Register select bit

DATA_OFFSET = 1  # Our Datapins have the mask 0b00011110, so shift data 1 to the left

# MCP23017 Register addresses
MCP23017_IODIRA = 0x00  # Set Input (1) or output (0) per pin, e.g. 0b11111111 tos et all input
MCP23017_IODIRB = 0x01
MCP23017_GPIOA = 0x12  # Register for the 'A' set of GPIO
MCP23017_GPIOB = 0x13


class I2C_Device:
    def __init__(self, addr, port=1):
        self.addr = addr
        self.bus = smbus.SMBus(port)

        # Set Bank B as output:
        self.bus.write_byte_data(ADDRESS, MCP23017_IODIRB, 0b00000000)

        # Set Bank A as output:
        self.bus.write_byte_data(ADDRESS, MCP23017_IODIRA, 0b00000000)

    # Write a single command
    def write_cmd_to(self, register, cmd):
        self.bus.write_byte_data(self.addr, register, cmd)
        time.sleep(0.001)

    # Write a single command
    def write_cmd(self, cmd):
        self.bus.write_byte_data(self.addr, MCP23017_GPIOB, cmd)  # LCD is on bank B
        time.sleep(0.001)

    # Read a single byte
    def read(self):
        return self.bus.read_byte(self.addr)

    # Read
    def read_data_from(self, register, cmd):
        return self.bus.read_byte_data(self.addr, register, cmd)


class LCD_Wrapper:
    # initializes objects and lcd
    def __init__(self):
        self.lcd_device = I2C_Device(ADDRESS)

        self.lcd_write(0x03)
        self.lcd_write(0x03)
        self.lcd_write(0x03)
        self.lcd_write(0x02)

        self.lcd_write(LCD_FUNCTIONSET | LCD_2LINE | LCD_5x8DOTS | LCD_4BITMODE)
        self.lcd_write(LCD_DISPLAYCONTROL | LCD_DISPLAYON)
        self.lcd_write(LCD_CLEARDISPLAY)
        self.lcd_write(LCD_ENTRYMODESET | LCD_ENTRYLEFT)
        time.sleep(0.2)

        self.lcd_backlight(True)

    # clocks EN to latch command
    def lcd_strobe(self, data):
        self.lcd_device.write_cmd_to(MCP23017_GPIOB, data | GPIOB_EN | ~LCD_BACKLIGHT_B)
        time.sleep(.005)
        self.lcd_device.write_cmd_to(MCP23017_GPIOB, ((data & ~GPIOB_EN) | ~LCD_BACKLIGHT_B))
        time.sleep(.001)

    def lcd_write_four_bits(self, data):
        self.lcd_device.write_cmd_to(MCP23017_GPIOB, data | ~LCD_BACKLIGHT_B)
        self.lcd_strobe(data)

    # write a command to lcd
    def lcd_write(self, cmd, mode=0):
        self.lcd_write_four_bits(mode | (cmd & 0xF0))
        self.lcd_write_four_bits(mode | ((cmd << 4) & 0xF0))

    def lcd_write_reversed(self, cmd, mode=0):
        # Our datapins are reversed, so reverse the bits:
        rev_cmd = cmd
        rev_cmd = ((rev_cmd & 0b01010101) << 1) & ((rev_cmd & 0b10101010) >> 1)
        rev_cmd = ((rev_cmd & 0b00110011) << 2) & ((rev_cmd & 0b11001100) >> 2)
        rev_cmd = ((rev_cmd & 0b00001111) << 4) & ((rev_cmd & 0b11110000) >> 4)

        first_cmd = cmd & 0xF0
        second_cmd = (cmd << 4) & 0xF0
        self.lcd_write_four_bits(mode | (first_cmd))
        self.lcd_write_four_bits(mode | (second_cmd))

    # turn on/off the lcd backlight
    def lcd_backlight(self, state):
        if state:
            self.lcd_device.write_cmd_to(MCP23017_GPIOA, ~(LCD_BACKLIGHT_R | LCD_BACKLIGHT_G))
            self.lcd_device.write_cmd_to(MCP23017_GPIOB, ~LCD_BACKLIGHT_B)
        else:
            self.lcd_device.write_cmd_to(MCP23017_GPIOA, LCD_BACKLIGHT_R | LCD_BACKLIGHT_G)
            self.lcd_device.write_cmd_to(MCP23017_GPIOB, LCD_BACKLIGHT_B)

    # turn on/off the lcd backlight
    def lcd_color(self, color):
        if isinstance(color, lcd.Color):
            r = LCD_BACKLIGHT_R if color.value[0] is 1 else 0x00
            g = LCD_BACKLIGHT_G if color.value[1] is 1 else 0x00
            b = LCD_BACKLIGHT_B if color.value[2] is 1 else 0x00
            self.lcd_device.write_cmd_to(MCP23017_GPIOA, ~(r | g))
            self.lcd_device.write_cmd_to(MCP23017_GPIOB, ~b)

    # put string function
    def lcd_display_string(self, string, line):
        if line == 1:
            self.lcd_write(0x80)
        if line == 2:
            self.lcd_write(0xC0)
        if line == 3:
            self.lcd_write(0x94)
        if line == 4:
            self.lcd_write(0xD4)

        for char in string:
            self.lcd_write(ord(char), GPIOB_RS)

    # clear lcd and set to home
    def lcd_clear(self):
        self.lcd_write(LCD_CLEARDISPLAY)
        self.lcd_write(LCD_RETURNHOME)


class AdafruitLCD(lcd.LCD):
    lcd = None

    def __init__(self, columns=16, rows=2):

        super(AdafruitLCD, self).__init__(rows=rows, columns=columns)
        self.lcd = LCD_Wrapper()
        self.clear()

    def write_line(self, string, row):
        self.lcd.cursor_pos = (row, 0)
        self.lcd.write_string(string)

    def loop_line(self, string, row, repeats, padding, delay=0.25):
        if row > self.rows:
            raise Exception("Illegal rows count")
        if repeats < 1:
            repeats = 1

        # Start with empty spaces:
        complete_string = ' ' * self.columns

        # Add text and padding
        for i in range(repeats - 1):
            complete_string += string + ' ' * padding

        # Add the last text + fill with empty spaces for complete fade-out
        complete_string += string + ' ' * self.columns

        for i in range(len(complete_string) - self.columns):
            buffer_string = complete_string[i:i + self.columns]
            self.write_line(buffer_string, row)
            time.sleep(delay)

        # Clear just the used line:
        self.clear_line(row)

    def clear_line(self, row):
        self.lcd.cursor_pos = (row, 0)
        self.lcd.write_string(' ' * self.columns)

    def close(self, clear):
        self.lcd.close(clear)

    def backlight(self, backlight):
        self.lcd.backlight_enabled = backlight

    def clear(self):
        self.lcd.clear()

    def color(self, null):
        pass


# Old version with adafruit:
"""
    def __init__(self, columns=16, rows=2):

        super(AdafruitLCD, self).__init__(rows=rows, columns=columns)

        self.i2c = busio.I2C(board.SCL, board.SDA)
        self.lcd = rgb_lcd.Character_LCD_RGB_I2C(self.i2c, columns, rows)
        self.lcd.cursor = False
        self.color(lcd.Color.RED)
        self.clear()

    def write_line(self, string, row):
        for i in range(0, row):
            string = "\n" + string
        self.lcd.message = string

    def loop_line(self, string, row, repeats, padding, delay=0.25):
        if row > self.rows:
            raise Exception("Illegal row")
        if repeats < 1:
            repeats = 1

        # Start with empty spaces:
        complete_string = ' ' * self.columns

        # Add text and padding
        for i in range(repeats - 1):
            complete_string += string + ' ' * padding

        # Add the last text + fill with empty spaces for complete fade-out
        complete_string += string + ' ' * self.columns

        for i in range(len(complete_string) - self.columns):
            buffer_string = complete_string[i:i + self.columns]
            self.write_line(buffer_string, row)

        # Clear just the used line:
        self.clear_line(row)

    def clear_line(self, row):
        string = ' ' * self.columns
        for i in range(0, row):
            string = "\n" + string
        self.lcd.message = string

    def close(self, clear):
        if clear:
            self.lcd.clear()
        self.lcd.display = False

    def backlight(self, backlight):
        self.lcd.backlight_enabled = backlight

    def clear(self):
        self.lcd.clear()

    def color(self, color):
        if isinstance(color, lcd.Color):
            self.lcd.color = color.value
"""
