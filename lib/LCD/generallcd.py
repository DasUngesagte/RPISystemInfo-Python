#!/usr/bin/python3
from lib.LCD.RPLCD.i2c import CharLCD as CharLCDi2c
from lib.LCD.RPLCD.gpio import CharLCD as CharLCDgpio
import time
from lib.LCD import lcd


class GeneralLCD(lcd.LCD):
    lcd = None

    def __init__(self, i2c=True, i2c_expander=None, i2c_address=None, columns=16, rows=2, dotsize=8,
                 gpio_rs=None, gpio_e=None, gpio_data=None,
                 charmap=None, backlight=True, auto_linebreaks=True):

        super(GeneralLCD, self).__init__(rows=rows, columns=columns)

        if i2c:
            self.lcd = CharLCDi2c(i2c_expander=i2c_expander, address=i2c_address,
                                  cols=columns, rows=rows, dotsize=dotsize,
                                  auto_linebreaks=auto_linebreaks,
                                  backlight_enabled=backlight)
        else:
            self.lcd = CharLCDgpio(pin_rs=gpio_rs, pin_e=gpio_e,
                                   pins_data=gpio_data,
                                   cols=columns, rows=rows, dotsize=dotsize,
                                   charmap=charmap,
                                   auto_linebreaks=auto_linebreaks)

        self.clear()

    def write_line(self, string, row):
        self.lcd.cursor_pos = (row, 0)
        self.lcd.write_string(string)

    def loop_line(self, string, row, repeats, padding, delay=0.25):
        if row > self.rows:
            raise Exception("Illegal rows count")
        if repeats < 1:
            repeats = 1

        # Start with empty spaces:
        complete_string = ' ' * self.columns

        # Add text and padding
        for i in range(repeats - 1):
            complete_string += string + ' ' * padding

        # Add the last text + fill with empty spaces for complete fade-out
        complete_string += string + ' ' * self.columns

        for i in range(len(complete_string) - self.columns):
            buffer_string = complete_string[i:i + self.columns]
            self.write_line(buffer_string, row)
            time.sleep(delay)

        # Clear just the used line:
        self.clear_line(row)

    def clear_line(self, row):
        self.lcd.cursor_pos = (row, 0)
        self.lcd.write_string(' ' * self.columns)

    def close(self, clear):
        self.lcd.close(clear)

    def backlight(self, backlight):
        self.lcd.backlight_enabled = backlight

    def clear(self):
        self.lcd.clear()

    def color(self, null):
        pass
