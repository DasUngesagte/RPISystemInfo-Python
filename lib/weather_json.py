#!/usr/bin/python
from collections import namedtuple
from urllib.request import urlopen
from urllib import error
import json

url = "https://maps.dwd.de/geoserver/dwd/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=dwd" \
      ":Warnungen_Gemeinden&outputFormat=text%2Fjavascript"

DATA = None
WARNINGS = []

# Structtype:
weather_event = namedtuple("weather_event", "event expires description severity")
weather_warning = namedtuple("weather_warning", "location weather_events")


def update(locations):
    global DATA

    try:
        response = urlopen(url)
        tmp_data = response.read().decode('utf-8')
        DATA = json.loads(tmp_data[len('parseResponse('):-1])
    except (error.URLError, error.HTTPError, error.ContentTooShortError):
        print("Download error")
        DATA = None

    search_warnings(locations)


def search_warnings(locations):
    global DATA, WARNINGS

    # We collect new warnings:
    WARNINGS.clear()

    found = False

    if DATA is None:
        return

    for position, feature in enumerate(DATA["features"]):
        for location in locations:
            if feature["properties"]["NAME"] == location or feature["properties"]["AREADESC"] == location:

                # Do we already have a warning for this location?
                for existing in WARNINGS:
                    if location == existing.location:
                        existing.weather_events.append(weather_event(event=feature["properties"]["EVENT"],
                                                                     expires=feature["properties"]["EXPIRES"],
                                                                     description=feature["properties"]["DESCRIPTION"],
                                                                     severity=feature["properties"]["SEVERITY"]))
                        found = True
                        break

                # Create a new warning:
                if not found:
                    WARNINGS.append(weather_warning(location=location,
                                                    weather_events=[weather_event(event=feature["properties"]["EVENT"],
                                                                                  expires=feature["properties"][
                                                                                      "EXPIRES"],
                                                                                  description=feature["properties"][
                                                                                      "DESCRIPTION"],
                                                                                  severity=feature["properties"]["SEVERITY"])]))

            # Reset for the next features/warning:
            found = False


# Looks nicer than a direct access:
def get_warnings():
    global WARNINGS
    return WARNINGS
