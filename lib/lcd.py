from enum import Enum
import time


class Color(Enum):
    BLACK = [0, 0, 0]
    RED = [100, 0, 0]
    GREEN = [0, 100, 0]
    BLUE = [0, 0, 100]
    YELLOW = [100, 100, 0]
    VIOLET = [100, 0, 100]
    TEAL = [0, 100, 100]
    WHITE = [100, 100, 100]


class LCD:
    lcd = None
    columns = 16
    rows = 2

    def __init__(self, columns=16, rows=2):
        self.columns = columns
        self.rows = rows

    def write_line(self, string, row):
        pass

    def loop_line(self, string, row, repeats, padding, delay=0.25):
        if row > self.rows:
            raise Exception("Illegal rows count")
        if repeats < 1:
            repeats = 1

        # Start with empty spaces:
        complete_string = ' ' * self.columns

        # Add text and padding
        for i in range(repeats - 1):
            complete_string += string + ' ' * padding

        # Add the last text + fill with empty spaces for complete fade-out
        complete_string += string + ' ' * self.columns

        for i in range(len(complete_string) - self.columns):
            buffer_string = complete_string[i:i + self.columns]
            self.write_line(buffer_string, row)
            time.sleep(delay)

        # Clear just the used line:
        self.clear_line(row)

    def clear_line(self, row):
        pass

    def close(self, clear):
        pass

    def backlight(self, backlight):
        pass

    def clear(self):
        pass

    def color(self, null):
        pass
