#!/usr/bin/python3
# -*- coding: utf-8 -*-
from lib import rpisysteminfo, keypad


# -------------------------------------------------------------------------------------------------------------------- #
#                                               Global-Settings                                                        #
#                                               Set in any case                                                        #
# -------------------------------------------------------------------------------------------------------------------- #

THREAD_NUMBER = 4  # Number of CPU threads
WEATHER_INFO = ["Nordkirchen", "Münster-Nord"]  # Municipality or city
SERVICES = []  # List of systemd-services to monitor
SHOW_PAGES_FOR = 5

COLUMNS = 16
ROWS = 2
DOTSIZE = 8

USE_ADAFRUIT = True

# -------------------------------------------------------------------------------------------------------------------- #
#                                               Adafruit-Settings                                                      #
#                                       Use only if USE_ADAFRUIT = True                                                #
# -------------------------------------------------------------------------------------------------------------------- #

USE_BUTTONS = True
BUTTON_ACTIONS = {keypad.Buttons.SELECT: keypad.Actions.TOGGLE_BACKLIGHT,  # See lib/keypad for possible values
                  keypad.Buttons.LEFT: keypad.Actions.NONE,
                  keypad.Buttons.RIGHT: keypad.Actions.NONE,
                  keypad.Buttons.UP: keypad.Actions.PAGE_UP,
                  keypad.Buttons.DOWN: keypad.Actions.PAGE_DOWN}

# -------------------------------------------------------------------------------------------------------------------- #
#                                             NON-Adafruit-Settings                                                    #
#                                       Use only if USE_ADAFRUIT = False                                               #
# -------------------------------------------------------------------------------------------------------------------- #

METHOD = True  # True = I2C, False = GPIO

# --------------------------------- #
#            I2C-Settings           #
# --------------------------------- #

I2C_EXPANDER = "MCP23017"
I2C_ADDRESS = 0x20
AUTO_LINEBREAKS = True
BACKLIGHT = True
NIGHT = [22, 6]  # Interval where the backlight will be off

# --------------------------------- #
#            GPIO-Settings          #
# --------------------------------- #

GPIO_RS = 17  # Use BCM numbering
GPIO_E = 17
GPIO_DATA = [17, 17, 17, 17]
CHARMAP = "A00"  # Default is "A00"


# ------------------------------------------- End of adjustable variables -------------------------------------------- #

if __name__ == "__main__":
    systeminfo = rpisysteminfo.RPISystemInfo()
    systeminfo.mainloop()
